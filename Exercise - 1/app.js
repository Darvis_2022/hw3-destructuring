const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const clients = clients1.concat(clients2);

const clientsFilter = (arr) => {
  const seen = {};
  const result = [];
  let j = 0;

  for(let i = 0; i < arr.length; i++) {
    const item = arr[i];
    const itemType = typeof item;
    const key = `${itemType}_${item}`;
    if (!seen[key]) {
      seen[key] = 1;
      result[j++] = item;
    }
  }

  console.log(result);

  return result;
}

clientsFilter(clients);